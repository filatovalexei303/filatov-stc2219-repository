public class HomeWork {
    public static void main(String[] args) {
        int[] arrayNumbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int x = 2;
        int numberPosition = SearchNumber(arrayNumbers,x);
        System.out.println(numberPosition);
    }
    public static int SearchNumber(int[] arrayNumbers,int x){
        int position = 0;
        for (int i = 0; i< arrayNumbers.length; i++){
            if(arrayNumbers[i] == x){
                position =i;
                break;
            }
            else{
                position = -1;
            }
        }
        return position;
    }
}
