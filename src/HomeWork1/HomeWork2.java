package HomeWork1;

import java.util.Arrays;

public class HomeWork2 {
    public static void main(String[] args){
        int[] arrayNumbers = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        replacementNumbers(arrayNumbers);

    }
    public static void replacementNumbers(int arrayNumbers[]){
        System.out.println(Arrays.toString(arrayNumbers));
        System.out.println("Сдвигаем все числа, чтобы между ними не было нолей :");
        int[] newArray = new int[arrayNumbers.length];
        int j=0;
        for (int i=0; i< arrayNumbers.length;i++){

            newArray[i]=0;
            if(arrayNumbers[i] != 0){
                newArray[j] = arrayNumbers[i];
                j++;
            }
        }
        System.out.println(Arrays.toString(newArray));
    }
}
