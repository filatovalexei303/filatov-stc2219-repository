package HomeWork1;

import java.util.Scanner;

public class HomeWork1 {
    public static void main(String[] args) {
        int[] arrayNumbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println("Введите число от 1 до 10");
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        while( x <0 ||  x >10) {
            System.out.println("Заданное число не входит в установленный диапазон");
            System.out.println("Введите число от 1 до 10");
            x = scanner.nextInt();
            }
            if (x>0 || x<=10) {
                System.out.println("Число в массиве стоит на индексе : " + SearchNumber(arrayNumbers, x));
            }

        }

    public static int SearchNumber(int[] arrayNumbers,int x){
        int position = 0;
        for (int i = 0; i< arrayNumbers.length; i++){
            if(arrayNumbers[i] == x){
                position =i;
                break;
            }
            else{
                position = -1;
            }
        }
        return position;
    }
}
