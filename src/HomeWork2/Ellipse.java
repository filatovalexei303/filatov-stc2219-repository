package HomeWork2;

public class Ellipse extends Figure{

    public Ellipse(String nameFigure,int x, int y) {
        super(nameFigure, x, y);
    }

    int a = 4; //длинна меньшей оси
    int b = 6; //длинна большей оси
    @Override
    void getPerimetr() { //метод для получения периметра фигуры
        double p = 2*Math.PI*Math.sqrt((a^2 + b^2)/2) ; //вычисляем площадь эллипса
       String result = String.format("%.2f",p); //форматируем полученное значение до 2х знаков после запятой
        System.out.println( getNameFigure()+ " координаты x:" + getX() + " y:" + getY()
                + " - периметр " + " равен : " + result); // при обращении к методу выводим на консоль координаты и периметр
    }
}
