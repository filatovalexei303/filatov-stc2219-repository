package HomeWork2;

public class Square extends Rectangle implements Moveable{
    public Square(String nameFigure, int x , int y) {
        super(nameFigure,x, y);
    }
    int a = 8; //задаем сторону квадрата

    @Override
    void getPerimetr() { //метод для получения периметра фигуры
        int p = a*4; // создаем переменную для хранения вычисляемого значения периметра фигуры
        System.out.println(getNameFigure() + " (подвижен) " +" координаты x:" + getX() + " y:" + getY()
                +  " - периметр " + " равен : " + p); // при обращении к методу выводим на консоль координаты и периметр
    }

    @Override
    public void move(int x, int y) { // метод перемещения фигур
        setX((int) (Math.random()*15)); // переопределяем коррдинату х на случайную от 0 до 15
        setY((int) (Math.random()*15)); // переопределяем коррдинату у  на случайную от 0 до 15
    }
}
