package HomeWork2;

public class Circle extends Ellipse implements Moveable{
    public Circle(String nameFigure, int x , int y) {
        super(nameFigure, x , y);
    }
    int r = 5; //задаем радиус круга
    @Override
    void getPerimetr() { // метод для вычисления и вавода периметра и координат фигуры
        double p = 2*Math.PI*r; // создаем переменную для хранения вычисляемого значения периметра фигуры
        String result = String.format("%.2f",p); // форматируем полученное значение до 2х знаков после запятой
        System.out.println(getNameFigure() + " (подвижен) "  +" координаты x:" + getX() + " y:" + getY()
                +  " - периметр " + " равен : "+result); // при обращении к методу выводим на консоль координаты и периметр
    }

    @Override
    public void move(int x, int y) { // метод перемещения фигур
        setX((int) (Math.random()*15)); // переопределяем коррдинату х на случайную от 0 до 15
        setY((int) (Math.random()*15)); // переопределяем коррдинату у  на случайную от 0 до 15

    }
}
