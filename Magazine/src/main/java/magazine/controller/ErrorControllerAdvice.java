package magazine.controller;

import org.apache.logging.log4j.message.Message;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ErrorControllerAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus (HttpStatus.INTERNAL_SERVER_ERROR)
    public String exeprion(Exception exception, Model model){
        String errorMessage = (exception != null ? exception.getMessage() : "Неизвестная Ошибка");
        model.addAttribute("errorMessage", errorMessage);
        return "error";
    }

}
