package magazine.controller;

import lombok.RequiredArgsConstructor;
import magazine.DTO.UserDTO;
import magazine.domain.User;
import magazine.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Objects;

@Controller
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;


    @GetMapping
    public String userList(Model model){

        model.addAttribute("users",userService.getAll());
        return "userList";
    }

    @GetMapping("/new")
    public String newUser(Model model){
        model.addAttribute("user",new UserDTO());
        return "user";
    }
    @PostMapping("/new")
    public String saveUser(UserDTO userDTO,Model model){
        if(userService.save(userDTO)){
            return "redirect:/users";
        }else {
            model.addAttribute("user",userDTO);
            return "user";
        }
    }
    @GetMapping("/reg")
    public String registration(Model model){
        model.addAttribute("user",new UserDTO());
        return "register";
    }

    @PostMapping("/reg")
    public String reg(UserDTO userDTO, Model model){
        if(userService.saveNewClient(userDTO)){

            return "redirect:/login";
        }else{
            model.addAttribute("regError",true);
            model.addAttribute("user",userDTO);
            return "register";
        }
    }

    @GetMapping("/profile")
    public String profileUser( Model model, Principal principal){
        if(principal == null){
            throw new RuntimeException("You are not identificated!");
        }
        User user = userService.findByName(principal.getName());

        UserDTO dto = UserDTO.builder()
                .username(user.getName())
                .email(user.getEmail())
                .build();
        model.addAttribute("user",dto);
        return "profile";
    }
    @PostMapping("/profile")
    public String updateProfileUser(UserDTO dto,Model model, Principal principal){
        if(principal==null || !Objects.equals(principal.getName(),dto.getUsername())){
            throw new RuntimeException("You are not identificated!");
        }
        if(dto.getPassword() != null && dto.getPassword().isEmpty()
                && !Objects.equals(dto.getPassword(), dto.getMatchingPassword())){
            model.addAttribute("user",dto);
            return "profile";
        }
        userService.updateProfile(dto);
        return "redirect:/";
    }

}
