package magazine.controller;

import magazine.DTO.ProductDTO;
import magazine.domain.Product;
import magazine.repository.ProductRepository;
import magazine.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/products")

public class ProductController {

    private final ProductService productService;
    private final ProductRepository productRepository;

    public ProductController(ProductService productService, ProductRepository productRepository) {
        this.productService = productService;
        this.productRepository = productRepository;

    }

    @GetMapping
    public String list(Model model){

            List<ProductDTO> list = productService.getAll();
            model.addAttribute("products", list);
            return "products";

    }

    @GetMapping("/{id}/bucket")
    public String addBucket(@PathVariable Long id, Principal principal){
        if(principal == null){
            return "redirect:/products";
        }
        productService.addToUserBucket(id,principal.getName());
        return "redirect:/products";
    }
    @GetMapping("/update{id}")
    public String profileProduct(@PathVariable Long id,Model model){

        Product newProduct =productRepository.findFirstById(id);

         ProductDTO dto = ProductDTO.builder()
                 .id(id)
                 .title(newProduct.getTitle())
                .price(newProduct.getPrice())
                .build();
        model.addAttribute("product",dto);
        return "productProfile";
    }
    @PostMapping("/update{id}")
    public String updateProductProfile(ProductDTO dto){
        productService.updateProduct(dto);
        return "redirect:/products";
    }
    @GetMapping("/newProduct")
    public String newProduct(Model model){
        model.addAttribute("product",new ProductDTO());
        return "product";
    }
    @PostMapping("/newProduct")
    public String saveProduct(ProductDTO productDTO){
        productService.saveProduct(productDTO);
            return "redirect:/products";

    }
    @GetMapping("/delete{id}")
    public String profileProduct(@PathVariable Long id){
        Product product = productRepository.findFirstById(id);
        productService.deleteProduct(product.getId());
        return "redirect:/products";
    }




}
