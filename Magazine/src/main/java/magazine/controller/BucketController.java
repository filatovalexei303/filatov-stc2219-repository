package magazine.controller;

import magazine.DTO.BucketDTO;
import magazine.repository.BucketRepository;
import magazine.service.BucketService;
import magazine.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


import java.security.Principal;


@Controller

public class BucketController {
    private final BucketService bucketService;

    public BucketController(BucketService bucketService) {
        this.bucketService = bucketService;
    }

    @GetMapping("/bucket")
    public String aboutBucket(Model model, Principal principal) {
        if (principal == null) {
            model.addAttribute("bucket", new BucketDTO());
        } else {
            BucketDTO bucketDto = bucketService.getBucketByUser(principal.getName());
            model.addAttribute("bucket", bucketDto);
        }
        return "bucket";

    }
   @GetMapping("/delete{productId}")
   public String delBucket(@PathVariable Long productId, Principal principal){


       bucketService.deleteProducts(productId,principal.getName());

       return "redirect:/bucket";
   }
   @PostMapping("/bucket")
    public String commit(Principal principal){
        if(principal != null){
            bucketService.commitBucketToOrder(principal.getName());
        }
        return "redirect:/bucket";
   }

}
