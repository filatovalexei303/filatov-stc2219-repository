package magazine.config;

import magazine.domain.Role;

import magazine.service.UserService;
import magazine.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;




@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true,prePostEnabled = true)
public class SecurityConfig  {

    @Autowired
    UserService userService;
@Autowired
UserServiceImpl userServiceImpl;

@Bean
    public PasswordEncoder passwordEncoder() {
        return
                new BCryptPasswordEncoder();
    }

    @Bean
 public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
     http.authorizeRequests()
             .antMatchers("/users").hasAnyAuthority(Role.ADMIN.name(),Role.MANAGER.name())
             .antMatchers("/users/new").hasAuthority(Role.ADMIN.name())
             .antMatchers("/products/newProduct").hasAnyAuthority(Role.ADMIN.name(),Role.MANAGER.name())
             .antMatchers("/")
             .permitAll()
             .and()
                .formLogin()
             .loginPage("/login")
             .failureUrl("/login-error")
                .loginProcessingUrl("/auth")
                .permitAll()
             .and()
                 .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                 .logoutSuccessUrl("/").deleteCookies("JSESSIONID")
                 .invalidateHttpSession(true)
                 .and()
             .csrf().csrfTokenRepository(new HttpSessionCsrfTokenRepository());
     return http.build();
 }



}
