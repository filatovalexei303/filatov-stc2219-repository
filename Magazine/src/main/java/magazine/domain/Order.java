package magazine.domain;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;

   @CreationTimestamp
    private LocalDateTime created;
   @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
   private BigDecimal sum;
    private String address;
   @Enumerated(EnumType.STRING)
    private OrderStatus status;



}
