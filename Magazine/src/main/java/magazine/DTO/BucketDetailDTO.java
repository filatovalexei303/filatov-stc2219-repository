package magazine.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import magazine.domain.Product;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BucketDetailDTO {
    private String title;
    private Long productId;
    private BigDecimal price;
    private BigDecimal amount;
    private double sum;

    public BucketDetailDTO(Product product){
        this.title=product.getTitle();
        this.productId=product.getId();
        this.price=product.getPrice();
        this.amount=new BigDecimal(1.0);
        this.sum=Double.valueOf(product.getPrice().toString());
    }
}
