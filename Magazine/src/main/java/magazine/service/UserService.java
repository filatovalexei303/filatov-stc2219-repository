package magazine.service;

import magazine.DTO.UserDTO;
import magazine.domain.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    boolean save(UserDTO userDTO);
    void save(User user);
    List<UserDTO> getAll();
    User findByName(String name);
    void updateProfile(UserDTO userDTO);
    boolean saveNewClient(UserDTO userDTO);
    boolean emailValidate(String email);
}
