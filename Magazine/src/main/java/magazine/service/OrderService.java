package magazine.service;

import magazine.domain.Order;

public interface OrderService {
    void saveOrder(Order order);
}
