package magazine.service;

import magazine.DTO.BucketDTO;
import magazine.DTO.BucketDetailDTO;
import magazine.domain.*;
import magazine.repository.BucketRepository;
import magazine.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service

public class BucketServiceImpl implements BucketService {
    private final BucketRepository bucketRepository;
    private final ProductRepository productRepository;
    private final UserService userService;
    private final OrderService orderService;

    public BucketServiceImpl(BucketRepository bucketRepository, ProductRepository productRepository, UserService userService, OrderService orderService) {
        this.bucketRepository = bucketRepository;
        this.productRepository = productRepository;
        this.userService = userService;


        this.orderService = orderService;
    }


    @Override
    @Transactional
    public Bucket creatBucket(User user, List<Long> productIds) {
        Bucket bucket = new Bucket();
        bucket.setUser(user);
        List<Product> productList = getCollectRefProductsByIda(productIds);
        bucket.setProducts(productList);
        return bucketRepository.save(bucket);
    }

    private List<Product> getCollectRefProductsByIda(List<Long> productIds) {
        return productIds.stream()
                .map(productRepository :: getOne)
                .collect(Collectors.toList());
    }

    @Override
    public void addProducts(Bucket bucket, List<Long> productIds) {

        List<Product> products=bucket.getProducts();
        List<Product> newProductList= products == null ? new ArrayList<>() : new ArrayList<>(products);
        newProductList.addAll(getCollectRefProductsByIda(productIds));
        bucket.setProducts(newProductList);
        bucketRepository.save(bucket);

    }
    @Override
    @Transactional
    public void deleteProducts(Long productId,String name) {
       User user = userService.findByName(name);
       List<Product> products = user.getBucket().getProducts();
        for(Product product : products) {
            if(product.getId().equals(productId)){
                products.remove(product);
                break;
            }
        }

        Bucket bucket = user.getBucket();
        bucket.setProducts(products);
        bucket.setUser(user);
        bucketRepository.save(bucket);

    }




    @Override
    public BucketDTO getBucketByUser(String name) {
        User user =userService.findByName(name);
        if(user == null || user.getBucket() == null){
            return new BucketDTO();
        }
        BucketDTO bucketDTO = new BucketDTO();
        Map<Long, BucketDetailDTO> mapByProductId = new HashMap<>();

        List<Product> products = user.getBucket().getProducts();
        for(Product product : products){
            BucketDetailDTO detail = mapByProductId.get(product.getId());
            if(detail == null){
                mapByProductId.put(product.getId(),new BucketDetailDTO(product));
            }
            else{
                detail.setAmount(detail.getAmount().add(new BigDecimal(1.0)));
                detail.setSum(detail.getSum() + Double.valueOf(product.getPrice().toString()));
            }
        }
        bucketDTO.setBucketDetails(new ArrayList<>(mapByProductId.values()));
        bucketDTO.aggregate();

        return bucketDTO;
    }

    @Override
    @Transactional
    public void commitBucketToOrder(String userName) {
        User user = userService.findByName(userName);
        if(user == null){
            throw new RuntimeException("User not found");
        }
        Bucket bucket = user.getBucket();
        Order order=new Order();
        order.setStatus(OrderStatus.NEW);
        order.setUser(user);

        Map<Product,Long> productWhithAmount = bucket.getProducts().stream()
                .collect(Collectors.groupingBy(product -> product,Collectors.counting()));
        List<OrderDetails> orderDetails = productWhithAmount.entrySet().stream()
                .map(pair -> new OrderDetails(order,pair.getKey(),pair.getValue()))
                .collect(Collectors.toList());

        BigDecimal total = new BigDecimal(orderDetails.stream()
                .map(detail->detail.getPrice().multiply(detail.getAmount()))
                .mapToDouble(BigDecimal :: doubleValue).sum());

        order.setSum(total);
        order.setAddress("none");

        orderService.saveOrder(order);
        bucket.getProducts().clear();
        bucketRepository.save(bucket);

    }



}
