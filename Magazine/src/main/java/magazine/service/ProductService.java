package magazine.service;

import magazine.DTO.ProductDTO;
import magazine.DTO.UserDTO;
import magazine.domain.Product;

import java.util.List;

public interface ProductService {
    List<ProductDTO> getAll();
    void addToUserBucket(Long productId, String username);
    void updateProduct(ProductDTO productDTO);
    boolean saveProduct(ProductDTO productDTO);
    void deleteProduct(Long id);

}



