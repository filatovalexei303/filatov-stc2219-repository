package magazine.service;

import magazine.DTO.UserDTO;
import magazine.domain.Role;
import magazine.domain.User;
import magazine.repository.UserReposytory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService{
    private final UserReposytory userReposytory;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserReposytory userReposytory, PasswordEncoder passwordEncoder) {
        this.userReposytory = userReposytory;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean save(UserDTO userDTO) {
       if(!Objects.equals(userDTO.getPassword(),userDTO.getMatchingPassword())){
           throw new NullPointerException("Password is not equal");
       }
       User user = User.builder()
               .name(userDTO.getUsername())
               .password(passwordEncoder.encode(userDTO.getPassword()))
               .email(userDTO.getEmail())
               .role(userDTO.getRole())
               .build();

       userReposytory.save(user);
       return true;
    }

    @Override
    public void save(User user) {
        userReposytory.save(user);
    }

    @Override
    public List<UserDTO> getAll() {
        return userReposytory.findAll()
                .stream()
                .map(this :: toDto)
                .collect(Collectors.toList());
    }

    @Override
    public User findByName(String name) {
        return userReposytory.findFirstByName(name);
    }

    @Override
    @Transactional
    public void updateProfile(UserDTO userDTO) {
        User savedUser = userReposytory.findFirstByName(userDTO.getUsername());
        if(savedUser == null){
            throw new RuntimeException("User " + userDTO.getUsername() + " not found!");
        }
        boolean isChanged = false;
        if(userDTO.getPassword() != null && !userDTO.getPassword().isEmpty()){
            savedUser.setPassword(passwordEncoder.encode(userDTO.getPassword()));
            isChanged = true;
        }
        if(!Objects.equals(userDTO.getEmail(),savedUser.getEmail())){
            savedUser.setEmail(userDTO.getEmail());
            isChanged =true;
        }
        if(isChanged){
            userReposytory.save(savedUser);
        }
    }

    @Override
    public boolean saveNewClient(UserDTO userDTO) {
        if(!Objects.equals(userDTO.getPassword(),userDTO.getMatchingPassword())){
            throw new RuntimeException("Password is not equal");
        }

        if(!emailValidate(userDTO.getEmail())){
            return false;
        }
        User user = User.builder()
                .name(userDTO.getUsername())
                .password(passwordEncoder.encode(userDTO.getPassword()))
                .email(userDTO.getEmail())
                .role(Role.CLIENT)
                .build();

        userReposytory.save(user);
        return true;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userReposytory.findFirstByName(username);
        if(user == null){
            throw new UsernameNotFoundException("User not found whith name: " + username);
        }
        List<GrantedAuthority> roles = new ArrayList<>();
        roles.add(new SimpleGrantedAuthority(user.getRole().name()));
        return new org.springframework.security.core.userdetails.User(
                user.getName(),
                user.getPassword(),
                roles);
    }
    private UserDTO toDto(User user){
        return UserDTO.builder()
                .username(user.getName())
                .role(user.getRole())
                .email(user.getEmail())
                .build();
    }
    @Override
    public boolean emailValidate(String email){
        List<User> users = userReposytory.findAll();
        for(User user : users){
            if(user.getEmail().equals(email)){
                return false;
            }
        }
        return true;
    }
}
