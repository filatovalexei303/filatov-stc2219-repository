package magazine.service;

import magazine.DTO.BucketDTO;
import magazine.DTO.BucketDetailDTO;
import magazine.DTO.ProductDTO;
import magazine.domain.Bucket;
import magazine.domain.Product;
import magazine.domain.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BucketService {
    Bucket creatBucket(User user, List<Long> productIds);
    void  addProducts(Bucket bucket, List<Long> productIds);
    void deleteProducts(Long productId,String name);

    BucketDTO getBucketByUser(String name);

    void commitBucketToOrder(String userName);



}
