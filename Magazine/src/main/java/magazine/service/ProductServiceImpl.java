package magazine.service;

import magazine.DTO.ProductDTO;
import magazine.domain.Bucket;
import magazine.domain.Product;
import magazine.domain.User;
import magazine.mapper.ProductMapper;
import magazine.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service

public class ProductServiceImpl implements ProductService {

    private final ProductMapper mapper = ProductMapper.MAPPER;
    private final ProductRepository productRepository;
    private final BucketService bucketService;
    private final UserService userService;

    public ProductServiceImpl(ProductRepository productRepository, BucketService bucketService, UserService userService) {
        this.productRepository = productRepository;
        this.bucketService = bucketService;
        this.userService = userService;
    }

    @Override
    public List<ProductDTO> getAll() {
        return mapper.fromProductList(productRepository.findAll());
    }

    @Override
    @Transactional
    public void addToUserBucket(Long id, String username) {
        User user = userService.findByName(username);
        if(user == null){
            throw  new RuntimeException("User not found " + username);
        }
        Bucket bucket = user.getBucket();
        if(bucket == null){
            Bucket newBucket = bucketService.creatBucket(user, Collections.singletonList(id));
            user.setBucket(newBucket);
            userService.save(user);
        }else {
            bucketService.addProducts(bucket,Collections.singletonList(id));

        }
    }
    @Override
    @Transactional
    public void updateProduct(ProductDTO productDTO) {
        Product updatingProduct = productRepository.findFirstById(productDTO.getId());
        if(updatingProduct == null){
            throw new RuntimeException("Product " + productDTO.getId() + " not found!");
        }
        boolean isChanged = false;
        if(productDTO.getPrice() != null ){
            updatingProduct.setPrice(productDTO.getPrice());
            isChanged = true;
        }
        if(!Objects.equals(productDTO.getTitle(),updatingProduct.getTitle())){
            updatingProduct.setTitle(productDTO.getTitle());
            isChanged =true;
        }
        if(isChanged){
            productRepository.save(updatingProduct);
        }
    }

    @Override
    public boolean saveProduct(ProductDTO productDTO) {
       Product product = Product.builder()
               .price(productDTO.getPrice())
               .title(productDTO.getTitle())
               .build();
        productRepository.save(product);
        return true;
    }

    @Override
    @Transactional
    public void deleteProduct(Long id) {
       Product product =productRepository.findFirstById(id);
        productRepository.delete(product);
    }


}



