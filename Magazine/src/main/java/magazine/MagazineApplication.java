package magazine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication

public class MagazineApplication {

    public static void main(String[] args) {
        //ConfigurableApplicationContext context =
       SpringApplication.run(MagazineApplication.class, args);
       // PasswordEncoder passwordEncoder =context.getBean(PasswordEncoder.class);
       // System.out.println(passwordEncoder.encode("123"));
    }

}
