package magazine.repository;

import magazine.domain.Bucket;
import magazine.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface BucketRepository extends JpaRepository<Bucket,Long> {

}
