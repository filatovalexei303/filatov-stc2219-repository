package magazine.repository;

import magazine.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserReposytory extends JpaRepository<User,Long> {
    User findFirstByName(String name);
}
