package magazine.service;

import magazine.domain.User;
import magazine.repository.UserReposytory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class UserServiceImplTest {
private  UserReposytory userReposytory;


@BeforeEach
void setUp(){
  userReposytory=Mockito.mock(UserReposytory.class);
}
    @Test
    void checkFindByName(){
        String name = "alex";
        User checkUser = User.builder()
                .id(1L)
                .name(name)
                .build();

        Mockito.when(userReposytory.findFirstByName(Mockito.anyString())).thenReturn(checkUser);

        User actualUser = userReposytory.findFirstByName(name);
        Assertions.assertNotNull(actualUser);
        Assertions.assertEquals(actualUser,checkUser);
    }
}