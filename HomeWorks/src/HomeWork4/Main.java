package HomeWork4;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        Rectangle rectangle = new Rectangle("Прямоугольник",5,6);
        Ellipse ellipse = new Ellipse( "Эллипс",7,9);
        Circle circle = new Circle("Круг",12,15);
        Square square = new Square("Квадрат",4,5);
        square.move(square.getX(), square.getY()); // вызываем метод move и меняем координаты на random
        circle.move(circle.getX(), circle.getY());// вызываем метод move и меняем координаты на random
        Figure[] figures = new Figure[]{rectangle,ellipse,circle,square};//создаем массив фигур
        System.out.println("Введите фигуру для вывода данных r,e,s,c либо all чтобы получить данные всех фигур");
        System.out.println("Чтобы выйти введите exit");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String answer = reader.readLine();
        while (!answer.equals("exit")) {
            if (answer.equals("r")) {
                figures[0].getPerimetr(); //расчитываем и выводим периметр и координаты прямоугольника
                answer = reader.readLine();
            }
            if (answer.equals("e")) {
                figures[1].getPerimetr(); // расчитываем и выводим периметр и координаты эллипса
                answer = reader.readLine();
            }
            if (answer.equals("s")) {
                figures[3].getPerimetr(); // расчитываем и выводим периметр и координаты квадрата
                answer = reader.readLine();
            }
            if (answer.equals("c")) {
                figures[2].getPerimetr(); // расчитываем и выводим периметр и координаты круга
                answer = reader.readLine();
            }
            if (answer.equals("all")) {
                figures[0].getPerimetr(); //расчитываем и выводим периметр и координаты прямоугольника
                figures[1].getPerimetr(); // расчитываем и выводим периметр и координаты эллипса
                figures[2].getPerimetr(); // расчитываем и выводим периметр и координаты круга
                figures[3].getPerimetr(); // расчитываем и выводим периметр и координаты квадрата
                answer = reader.readLine();
            } else {
                System.out.println("Неверный формат данных");
                answer = reader.readLine();
            }
        }
        System.out.println("Спасибо что пользуетесь нашей программой:)");
        reader.close();
    }

}
