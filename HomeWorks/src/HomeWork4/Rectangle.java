package HomeWork4;

public class Rectangle extends Figure{
    public Rectangle(String nameFigure, int x , int y) {
        super(nameFigure,x,y);
    }

    int a= 4; // сторона прямоугольника
    int b = 6; //сторона прямоугольника
    @Override
    void getPerimetr() {
        int p = 2*(a+b); //вычисляем периметр прямоугольника
        System.out.println(getNameFigure() +" координаты x:" + getX() + " y:" + getY()
                +  " - периметр " + " равен : "+p); // выводим полученной значение(периметр)
    }
}
