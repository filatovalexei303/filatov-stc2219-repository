package HomeWork7;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UserReposytoryImpl implements UserReposytory {
    private final String fileName;

    public UserReposytoryImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public User FindById(int id) {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int currentId = Integer.parseInt(parts[0]);
                if (currentId == id) {
                    String name = parts[1];
                    String lastName = parts[2];
                    int age = Integer.parseInt(parts[3]);
                    boolean hasWork = Boolean.parseBoolean(parts[4]);
                    User newUser = new User(id, name,lastName, age, hasWork);
                    users.add(newUser);
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        if (users.size() > 0) {
            return users.get(0);
        }
        else {
            return null;
        }
    }

    @Override
    public void creat(User user) {
       List<User> users = new ArrayList<>();
       Reader reader = null;
       BufferedReader bufferedReader = null;
       try {
           reader = new FileReader(fileName);
           bufferedReader = new BufferedReader(reader);
           String line = bufferedReader.readLine();
           while (line != null) {
               String[] parts = line.split("\\|");
               int id = Integer.parseInt(parts[0]);
               String name = parts[1];
               String lastName = parts[2];
               int age = Integer.parseInt(parts[3]);
               boolean hasWork = Boolean.parseBoolean(parts[4]);
               User newUser = new User(id, name, lastName, age, hasWork);
               users.add(newUser);
               line = bufferedReader.readLine();
           }
       } catch (IOException e) {
           throw new IllegalArgumentException(e);
       }
       User appUser = new User(user.id, user.firstName, user.lastName, user.age, user.isHasWork());
       if(FindById(appUser.getId()) == null){
           users.add(appUser);
       }
       else{
           System.out.println("Пользователь с таким id уже существует");
       }
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            for(User line : users)
            writer.write(line.toString() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public void update(User user) {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {

                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);

                String name = parts[1];
                String lastName = parts[2];
                int age = Integer.parseInt(parts[3]);
                boolean hasWork = Boolean.parseBoolean(parts[4]);
                if (id == user.getId()) {
                    User updateUser = new User(id, user.firstName, user.lastName, user.age, user.hasWork);
                    users.add(updateUser);
                }else {
                    User newUser = new User(id,name,lastName,age,hasWork);
                    users.add(newUser);
                }
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        if(FindById(user.getId()) == null) {
            System.out.println("Пользователь с таким id не найден");
        }
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            for(User line : users)
                writer.write(line.toString() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int currentId = Integer.parseInt(parts[0]);

                    String name = parts[1];
                    String lastName = parts[2];
                    int age = Integer.parseInt(parts[3]);
                    boolean hasWork = Boolean.parseBoolean(parts[4]);
                    User newUser = new User(currentId, name,lastName, age, hasWork);
                if (currentId == id) {
                    System.out.println(newUser + " Пользователь удален");
                }
                else {
                    users.add(newUser);
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            for(User line : users)
                writer.write(line.toString() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
