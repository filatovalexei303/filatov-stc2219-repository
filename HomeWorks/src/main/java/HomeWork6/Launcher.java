package HomeWork6;

import java.util.Arrays;
import java.util.Scanner;

public class Launcher {
    public static String answer; // строка  команды
    public static void launch(){ // метод для вывода меню и получения команды
        System.out.println("Какие проверки будем выполнять со списком чисел?");
        System.out.println("Введите команду :");
        System.out.println("Четные числа -c ");
        System.out.println("Нечетные числа -n");
        System.out.println("Является ли сумма цифр числа четным числом -sc");
        System.out.println("Проверка каждой цифры числа на четность -pc");
        System.out.println("Проверка числа на палиндромность - cpo");
        System.out.println("Для выходы введите exit");
        Scanner scanner = new Scanner(System.in);//считываем с клавиатуры введенную команду
         answer = scanner.next();// присваиваем ввденую команду переменной

    }
}
