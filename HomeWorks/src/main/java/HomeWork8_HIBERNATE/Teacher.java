package HomeWork8_HIBERNATE;

import lombok.*;


import javax.persistence.*;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter@Setter
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String lastname;
    private String subject;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tech_stud",
    joinColumns = {@JoinColumn(name = "teacher_id")},
    inverseJoinColumns = {@JoinColumn(name = "student_id")})
    private Set<Student> students;
}
