package HomeWork8_HIBERNATE;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FindTeacher {
    public static void findTecher(){
        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/HomeWork8",
                    "postgres","123456");
            String str="SELECT id FROM teacher";
            PreparedStatement preparedStatement = connection.prepareStatement(str);
            ResultSet result = preparedStatement.executeQuery();
            List<Integer> list = new ArrayList<>();
            while (result.next()){
                list.add(result.getInt(1) );
            }
            System.out.println("Введите id студента из списка : " + list.toString());
            Scanner scanner = new Scanner(System.in);
            int id = scanner.nextInt();
            int maxId = list.get(0);
            for(int i = 0; i<list.size();i++) {
                if(list.get(i)>maxId){
                    maxId = list.get(i);
                }
                if (list.get(i) == id) {
                    String sql = "SELECT \n" +
                            "\n" +
                            "tech_stud.student_id , teacher.lastName,teacher.subject From teacher\n" +
                            " JOIN tech_stud On tech_stud.teacher_id = teacher.id WHERE tech_stud.student_id =? ;";
                    PreparedStatement statement = connection.prepareStatement(sql);
                    statement.setInt(1, id);

                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        System.out.println("Студент id: " + resultSet.getString(1) + ", " +
                                "преподаватель: " +
                                resultSet.getString(2) + " " +
                                resultSet.getString(3));

                    }
                }

            }
            if(id> maxId){
                System.out.println("Студент с таким id не найден!");
            }
            connection.close();




        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
