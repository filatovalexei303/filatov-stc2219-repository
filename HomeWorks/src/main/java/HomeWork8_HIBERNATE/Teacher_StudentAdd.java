package HomeWork8_HIBERNATE;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class Teacher_StudentAdd {
    public static void Add() {
        try {
            Session session = HibernateUtil.getSession();
            Transaction transaction = session.getTransaction();
            transaction.begin();
            Student student = Student.builder()
                    .name("Иван")
                    .lastName("Петров")
                    .build();
            session.save(student);
            Student student1 = Student.builder()
                    .name("Дмитрий")
                    .lastName("Иванов")
                    .build();
            session.save(student1);
            Student student2 = Student.builder()
                    .name("Алексей")
                    .lastName("Попов")
                    .build();
            session.save(student2);
            Set<Student> student12 = new HashSet<>();
            student12.add(student1);
            student12.add(student2);
            Set<Student> students2 = new HashSet<>();
            students2.add(student);
            students2.add(student2);
            Set<Student> students1 = new HashSet<>();
            students1.add(student);
            students1.add(student1);
            Teacher teacher = Teacher.builder()
                    .lastname("Серов")
                    .subject("История")
                    .students(student12)
                    .build();
            session.save(teacher);
            Teacher teacher1 = Teacher.builder()
                    .lastname("Богатов")
                    .subject("Черчение")
                    .students(students1)
                    .build();
            session.save(teacher1);
            Teacher teacher2 = Teacher.builder()
                    .lastname("Орлов")
                    .subject("Информатика")
                    .students(students2)
                    .build();
            session.save(teacher2);
            transaction.commit();
            session.close();

        } catch (Throwable e) {
            e.printStackTrace();
        }


    }
}
