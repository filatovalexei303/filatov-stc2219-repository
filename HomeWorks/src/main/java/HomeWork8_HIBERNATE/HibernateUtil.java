package HomeWork8_HIBERNATE;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory session;
static {
    try{
        Configuration configuration = new Configuration().configure();
       session = configuration.buildSessionFactory();
    }catch (Throwable e){
    throw new ExceptionInInitializerError(e);
    }
}
public static Session getSession(){
return session.openSession();
}
}
