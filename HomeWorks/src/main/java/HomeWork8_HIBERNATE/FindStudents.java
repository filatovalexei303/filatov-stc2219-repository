package HomeWork8_HIBERNATE;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class FindStudents {
    public static void findStudents(){

        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/HomeWork8",
                    "postgres","123456");
            String str="SELECT id FROM student";
            PreparedStatement preparedStatement = connection.prepareStatement(str);
            ResultSet result = preparedStatement.executeQuery();
            List<Integer> list = new ArrayList<>();
            while (result.next()){
                list.add(result.getInt(1) );
            }
            System.out.println("Введите id преподавателя из списка : " + list.toString());
            Scanner scanner = new Scanner(System.in);
            int id = scanner.nextInt();
            int maxId = list.get(0);
            for(int i = 0; i<list.size();i++) {
                if(list.get(i)>maxId){
                    maxId = list.get(i);
                }
                if (list.get(i) == id) {
                    String sql = "SELECT \n" +
                            "\n" +
                            "tech_stud.teacher_id , student.name,student.lastName,student.id From student\n" +
                            " JOIN tech_stud On tech_stud.student_id = student.id WHERE tech_stud.teacher_id =? ";
                    PreparedStatement statement = connection.prepareStatement(sql);
                    statement.setInt(1, id);

                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {
                        System.out.println("Преподаватель id: " + resultSet.getString(1) + ", " +
                                "студент: " +
                                resultSet.getString(2) + " " +
                                resultSet.getString(3));

                    }
                }

                }
            if(id> maxId){
                System.out.println("Преподаватель с таким id не найден!");
            }
            connection.close();




        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
