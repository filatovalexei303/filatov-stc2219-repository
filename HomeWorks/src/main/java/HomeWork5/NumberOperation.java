package HomeWork5;

public class NumberOperation {
    public static int sumNumbers(int... number) {
        int sum = 0;
        for (int i = 0; i < number.length; i++) {
            sum = sum + number[i];
        }
        System.out.println("Результат сложения чисел равен : " + sum);
        return sum;
    }

    public static int subtractionNumbers(int... number) {
        int result = number[0];
        for (int i = 1; i < number.length; i++) {
            result = result - number[i];
        }
        System.out.println("Результат вычитания чисел от самого большого равен : " + result);
        return result;
    }

    public static int divisionNumbers(int... number) {
        int result = number[0];
        for (int i = 1; i < number.length; i++) {

            if (result < number[i]) {
                System.out.println("Делитель больше делимого! Дальше делить не будем, результат : " + result);
                return result;

            }
            if (number[i] < 0) {
                System.out.println("Делитель отрицательное число, дальше делить не будем, результат : " + result);
                return result;
            } else {
                result = result / number[i];
            }
        }
        System.out.println("Результат деления : " + result);
        return result;
    }
   public static void Factorial(){
      System.out.println("Введите число для вычисления факториала : ");
     int number = getArray.oneNumber();
      if (number < 0) {
          System.out.println("Нельзя вычислить факториал для отрациательного числа!");
      }else{
          long result = getFactorial(number);
          System.out.println("Факториал числа " + number + " равен " + result);
      }
   }
    private static int getFactorial(int number) {

        if (number == 0 || number == 1) {
            return number;
        }

        return number * getFactorial(number - 1);
    }
}
