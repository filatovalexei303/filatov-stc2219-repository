package HomeWork5;


import java.util.Scanner;

public  class Main  { // наследуемся от класса NumberOperation, тк будем реализовывать все его методы
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); // создаем объект типа scsnner для считывания данных с клавиатуры

        LauncherMain.getQuestion(); // вызываем меню управления программой(список команд для пользователя
        String answer = scanner.next(); // считываем введеную команду
        while (!answer.equals("exit")) { //запускаем цикл работы программы пока пользователь не введет exit

            if (answer.equals("-s")) { // если пользовател ввел команду
                int[] numbers = getArray.newArray(); // создаем массив и через метод newArray заполняем и сортируем его
                NumberOperation.sumNumbers(numbers); // вызываем метод сложения и получаем результат с выводом на консоль
                LauncherMain.getQuestion(); // вызываем меню управления
                answer = scanner.next(); // считываем ответ
            }
            if (answer.equals("-v")) { // если пользовател ввел команду
                int[] numbers = getArray.newArray();// создаем массив и через метод newArray заполняем и сортируем его

                NumberOperation.subtractionNumbers(numbers); // вызываем метод вычитания и выводим результат
                LauncherMain.getQuestion(); // вызываем меню управления
                answer = scanner.next(); // считываем ответ
            }
            if (answer.equals("-d")) { // если пользовател ввел команду
                int[] numbers = getArray.newArray(); // создаем массив и через метод newArray заполняем и сортируем его

                NumberOperation.divisionNumbers(numbers); // вызываем метод деления и выводим результат
                LauncherMain.getQuestion(); // вызываем меню управления
                answer = scanner.next(); // считываем ответ

            }
            if (answer.equals("-f")) { // если пользовател ввел команду
                NumberOperation.Factorial(); //вызываем метод вычисления факториала
                LauncherMain.getQuestion(); // вызываем меню управления
                answer = scanner.next(); // считываем ответ
            }
            if(answer.equals("exit")){ // если пользовател ввел команду
                System.out.println("Конец работы приложения."); //уведомляем о конце работы приложения
                scanner.close(); //закрываем поток чтения данных
            }
            else { // если пользовател ввел команду не из предложенных
                System.out.println("Неверная команда, попробуйте еще раз!");// выводим сообщение об ошибке ввода
                LauncherMain.getQuestion(); // вызываем меню управления
                answer = scanner.next(); // считываем ответ
            }
        }
    }

    private static class LauncherMain { // вложенный класс
        public static void getQuestion() { //метод вызова меню старта
            System.out.println(" ********************* "); // граница, чтобы вывод не сливался
            System.out.println("Введите код выполняемой операции : ");
            System.out.println("Сложение чисел : -s");
            System.out.println("Вычитание чисел : -v");
            System.out.println("Деление чисел : -d");
            System.out.println("Получить факториал числа : -f");
            System.out.println("Для выхода введите : exit");
        }
    } //класс стартого меню,вложенный
}
