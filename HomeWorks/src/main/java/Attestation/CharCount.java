package Attestation;

import HomeWork7.Launcher;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CharCount {
    public void chars() throws IOException {
        Map<String, Integer> map = new HashMap<>();

        try {
            BufferedReader file = new BufferedReader(new FileReader("Text.txt"));
            String line;

            while (file.ready()) {
                line= file.readLine();
                String[] splittedString = line.replaceAll("[^\\da-zA-Zа-яёА-ЯЁ ]", "").split("");

                for (int i = 0; i < splittedString.length; i++) {
                    if (map.containsKey(splittedString[i])) {
                        map.put(splittedString[i], map.get(splittedString[i]) + 1);
                    } else {
                        map.put(splittedString[i], 1);
                    }
                }
            }
        }catch(FileNotFoundException e){
                throw new RuntimeException();
            }
        File file = new File("NewFileSymbols.txt");
        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file));

            writer.write("Буквы встречаются в тексте следующее количество раз :" + "\n");
            for (Map.Entry<String, Integer> entry :
                    map.entrySet()) {

                writer.write(entry.getKey() + " - "
                        + entry.getValue());
                writer.newLine();
            }

            writer.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {

                writer.close();
            }
            catch (Exception e) {
            }
        }

        }
    }


