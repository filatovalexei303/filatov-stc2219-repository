package Attestation;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordCount {
    public void words()  {
        List<String> list = new ArrayList<>();
        Map<String,Integer> map = new HashMap<>();

       try{ FileReader file =new FileReader("Text.txt");
        String s = new String();
        char ch;
        while (file.ready()) {
            ch = (char)file.read();
            if (ch == '\n' || ch == ' ' || ch == ',') {
                list.add(s.toString());
                s = new String();
            }
            else {
                s += ch;
            }
        }
        if (s.length() > 0) {
            list.add(s.toString());
        }
        String[] array
                = list.toArray(new String[0]);

        for(int i =0; i< array.length; i++){
            if(map.containsKey(array[i])){
                map.put(array[i],map.get(array[i])+1 );
            }
            else{
                map.put(array[i], 1  );
            }
        }
       }catch (FileNotFoundException e){
           throw new RuntimeException();
       } catch (IOException e) {
           throw new RuntimeException(e);
       }
        File file = new File("NewFileWords.txt");
        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file));

            writer.write("Слова встречаются в тексте следующее количество раз :" + "\n");
            for (Map.Entry<String, Integer> entry :
                    map.entrySet()) {

                writer.write(entry.getKey() + " - "
                        + entry.getValue());
                writer.newLine();
            }

            writer.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {

                writer.close();
            }
            catch (Exception e) {
            }
        }
    }
}
