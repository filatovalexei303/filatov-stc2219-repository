package Attestation;

import java.io.IOException;
import java.util.Scanner;


public class Main {

    public static void main(String[] args)  {
       WordCount wordCount = new WordCount();
        CharCount charCount = new CharCount();
        Scanner scanner = new Scanner(System.in);
        String answer;
        System.out.println("Что будем считать?");
        System.out.println("Слова введите : w");
        System.out.println("Буквы введите : s");
        System.out.println("Для выхода введите : exit");
        answer = scanner.next();
        if(answer.equals("w")){
           wordCount.words();
            System.out.println("Слова посчитаны и записаны в файл : NewFileWords.txt");
        }
        if(answer.equals("s")) {


            try {
                charCount.chars();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            System.out.println("Буквы посчитаны и записаны в файл : NewFileSymbols.txt");
        }
        if (answer.equals("exit")){
            System.out.println("Конец работы приложения.");
        }

    }
}
