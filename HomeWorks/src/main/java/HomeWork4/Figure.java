package HomeWork4;

public abstract class Figure {
    private String nameFigure; //добавил имя фигуры (вне задания) для удобства
    private int x; // поле координаты
    private int y; // поле координаты

    public Figure(String nameFigure, int x, int y) { // конструктор абстрактного класса
        this.nameFigure = nameFigure;
        this.x = x;
        this.y = y;
    }
    abstract void getPerimetr(); // метод для вычисления периметра

    public String getNameFigure() { // геттер имени фигуры
        return nameFigure;
    }

    public void setNameFigure(String nameFigure) { //сеттер имени фигуры
        this.nameFigure = nameFigure;
    }

    public int getX() { // геттер координаты х
        return x;
    }

    public void setX(int x) { // сеттер координаты х
        this.x = x;
    }

    public int getY() { // геттер координаты у
        return y;
    }

    public void setY(int y) { // сеттер координаты у
        this.y = y;
    }
}
