package HomeWork6;

import java.util.Arrays;
import java.util.Random;


public class Main {
    public static void main(String[] args) {
        Random random = new Random(); // вызываем метод random
        int[] array = new int[10]; // создаем массив на 10 чисел
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(30) + 1; // присваиваем каждому индексу рандомное число,
        }                                            // bound устанавливает предел значений рандома.
        Launcher.launch(); // вызываем меню команд
        String answer= Launcher.answer; // создаем строку с введной командой
        while (!(answer.equals("exit")|| answer.equals("-c")||
                answer.equals("-n")||answer.equals("-pc")|| // проверяем команды на совместимость с возможными
                answer.equals("-sc")|| answer.equals("-cpo"))){
            System.out.println("Неверная команда. Повторите ввод"); //в случае неверной команды просим повторить ввод
            Launcher.launch(); //снова вызываем меню (в качестве подсказки)
            answer= Launcher.answer; // переопределяем переменную ответа в зависмости от команды и снова сравниваем
        }
        if(answer.equals("exit")){
            System.out.println("Работа приложения закончена"); // сообщение о конце работы программы
        }else {

            System.out.println("Заданный массив чисел " + Arrays.toString(array)); // если комда верная выводим сформированный массив
        }

        if (Launcher.answer.equals("-c")) {
            System.out.println("Четные числа : ");// ищем четные числа в массиве
            Sequence.filter(array,number -> { // перебираем значения массива  и проверяем соответствует ли значение
                if ( number % 2 == 0) {  // заданным параметрам
                    System.out.print(number); // выводим если соответствует
                    System.out.print(" ");
                    Sequence.check++;
                }
                return true;
            });
            if (Sequence.check == 0){
                System.out.println("Четных чисел в массиве нет");
            }
        }
        if (Launcher.answer.equals("-n")) {// ищем нечетные числа в массиве
            System.out.println("Нечетные числа : ");
            Sequence.filter(array,number -> {// перебираем значения массива  и проверяем соответствует ли значение
                if (number % 2 != 0) { // заданным параметрам
                    System.out.print(number); // выводим если соответствует
                    System.out.print(" ");
                    Sequence.check++;
                }
                return true;
            });
            if (Sequence.check == 0){
                System.out.println("Нечетных чисел в массиве нет");
            }
        }
        if(Launcher.answer.equals("-pc")){ // ищем четные цифры в массиве
            System.out.println("Четные цифры в массиве чисел : ");
            int[] newArray = new int[3]; //создаем временный массив куда будем ложить цифры
        for(int i =0; i< array.length; i++){ // перебираем значения массива
            newArray =numbersToArray(array[i]); // переводим цифры в массив чисел с помощью метода numbersToArray
            Sequence.filter(newArray,number -> { // передаем каждый новый массив цифр в метод filter
                if(number!=0 && number%2 ==0){ // проверяем соответствует ли число массива заданным параметрам
                    System.out.print(number + " "); // выводим если соответствует
                    Sequence.check++;
                }
                return true;

            });
            if (Sequence.check == 0){
                System.out.println("Четных цифр в массиве нет");
            }
        }
        }
        if(Launcher.answer.equals("-sc")){ // ищем четную сумму цифр каждого числа массива
            int[] sum = new int[10]; // вводим новый массив для сумм цифр
            int[] newArray = new int[3]; // вводим массив для разбивки числа на цифры
            for(int i = 0; i< array.length;i++){ //перебираем массив
                newArray = numbersToArray(array[i]); // переводим каждое число массива в массив цифр
                sum[i]= sumNumbers(newArray); // суммируем цифры в массиве
            }
            System.out.println("Четная сумма цифр каждого числа массива : ");
            Sequence.filter(sum,number -> {
                if (number % 2 == 0) { // проверяем на четность
                    System.out.print(number); // выводим если четное
                    System.out.print(" ");
                    Sequence.check++;
                }
                return true;
            });
            if(Sequence.check == 0){
                System.out.println("Четных сумм цифр в массиве нет");
            }
        }
        if(Launcher.answer.equals("-cpo")){ // проверяем на полиндромность
          Sequence.filter(array, number -> { // передаем массив чисел в метод filter
                    int rev=0;
                    int i = 0;
                    int num = number;
                    while(number!=0){ // цикл для проверки цифр числа на равенство
                        rev = number % 10;
                        i = i*10 + rev;
                        number = number / 10;
                    }
                    if(num>10 && num == i){ // проверяем что число больше двух знаков и что число равно числу наоборот
                        System.out.println("Число " + num + " это полиндром");// выводим число и сообщение что это полиндром
                        Sequence.check++; // добавляем 1 к счетчику
                    }
                    return true;
            });
          if(Sequence.check == 0){ // если счетчик равен 0 значит полиндромов не было в массиве
              System.out.println("Здесь нет чисел полиндромов"); // выводим сообщение об отстутствии
          }
        }
    }
    private static int[] numbersToArray(int number){ // метод перевода числа в массив чисел
        String s = Integer.toString(number); // переводим число в строку, чтобы определить длинну массива чисел
        int[] arr = new int[s.length()]; // определяем новый массив длинной в длинну строки
        for (int i = 0; i< arr.length; i++){ // проходим по созданному массиву
            arr[i] = number%10; // кладем остаток от деления в новый массив на новый индекс
            number/=10; // делим число на 10
        }
        return arr; // возвращаем вместо числа массив чисел
    }
    private static int sumNumbers(int[] array){ // метод суммирования чисел массива
        int result = 0; // объявляем переменную суммы
        for (int i = 0; i< array.length;i++){ // проходим по массиву
            result = result+ array[i]; // прибавляем к результату значение мссива на индексе i
        }
        return result;// возвращаем результат
    }

}
