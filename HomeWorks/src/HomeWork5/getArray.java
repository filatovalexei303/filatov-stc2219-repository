package HomeWork5;

import java.util.Scanner;

public class getArray {
    public static int[] newArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите пять чисел : ");
        int[] numbers = new int[5];
        for (int i = 0; i < numbers.length; i++) {
            int someNumber = scanner.nextInt();
            numbers[i] = someNumber;
        }
        Sort.sortNumbers(numbers);
        return numbers;
    }
    public static int oneNumber(){
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        return number;
    }
    private class Sort {
        public static void sortNumbers(int... number) {
            for (int i = 0; i < number.length; i++) {
                for (int j = i + 1; j < number.length; j++) {
                    if (number[j] > number[i]) {
                        int temp = number[i];
                        number[i] = number[j];
                        number[j] = temp;
                    }
                }
            }
        }
    }
}
